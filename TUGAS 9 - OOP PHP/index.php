<?php
require_once('animalia.php');
 require_once('frog.php');
require_once('ape.php');

$sheep = new Animalia("shaun");

echo "Nama Hewan : ". $sheep->name."<br>"; // "shaun"
echo "Jumlah kaki : ".$sheep->legs. "<br>"; // 4
echo "Berdarah dingin : ".$sheep->cold_blooded. "<br>"; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama Hewan : ".$kodok->name."<br>";
echo "jumlah kaki : ".$kodok->legs."<br>";
echo "Berdarah dingin : ".$kodok->cold_blooded. "<br>";
echo $kodok->jump(). "<br>"; // "hop hop"
echo "<br>";
$sungokong = new Ape("kera sakti");
echo "Nama Hewan : ".$sungokong->name."<br>";
echo "Jumlah Kaki : ".$sungokong->legs."<br>";
echo "Berdarah dingin : ".$sungokong->cold_blooded. "<br>";
$sungokong->yell() // "Auooo"



?>