@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn sm mb-4">Tambah Peran</a>
<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
      <tr>
        <td>{{$key+1}}</td>
        <td>{{$item -> nama}}</td>
        <td>{{$item -> umur}}</td>
        <td>{{$item -> bio}}</td>
        <td>
            
            <form action="/cast/{{$item-> id}}" method="POST">
                <a href="/cast/{{$item->id}}" class="btn btn-info btn sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn sm ml-2">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn sm ml-2" value="Delete">
            </form>
        </td>
    </tr>
    </tbody>
      @empty
      <tbody> 
        <tr>
            <td>Tidak ada di tabel peran</td>
        </tr>  
      @endforelse
    </tbody>
  </table>

@endsection