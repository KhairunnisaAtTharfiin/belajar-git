@extends('layout.master')
@section('judul')
    <h1>Halaman Update Cast</h1>
@endsection
@section('content')
<form action="/cast/{{$cast -> id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pemeran</label>
      <input type="text" name="nama" value="{{old('nama', $cast->nama)}}" class="form-control">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Umur Pemeran</label>
        <input type="text" name="umur" class="form-control" value="{{old('umur', $cast->umur)}}">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{old('bio', $cast->bio)}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection