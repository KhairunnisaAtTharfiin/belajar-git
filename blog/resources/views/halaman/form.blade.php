@extends('layout.master')
@section('judul')
    Halaman Daftar
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form method="POST" action="/kirim">
    @csrf
    <label>First Name:</label><br><br>
    <input type="text" name="namadepan"><br><br>
    <label>Last Name:</label><br><br>
    <input type="text" name="namabelakang"><br><br>
    <label> Gender: </label><br><br>
    <input type="radio">Male <br>
    <input type="radio">Female <br>
    <input type="radio">Other <br><br>
    <label >Nationality:</label><br><br>
    <select name="nationality" >
        <option value="indonesia">Indonesian</option>
        <option value="malaysia">Malaysian</option>
        <option value="singapura">Singapore</option>
        <option value="australia">Australia</option>
    </select><br><br>
    <label >Language Spoken:</label><br><br>
    <input type="checkbox" name="language"> Bahasa Indonesia <br>
    <input type="checkbox" name="language"> English <br>
    <input type="checkbox" name="language"> Other <br><br>
    <label >Bio:</label><br><br>
    <textarea name="pesan" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up"  >
    
</form>
@endsection
