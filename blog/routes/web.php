<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', 'HomeController@utama');
Route::get('/register', 'AuthController@daftar');
Route::post('/kirim','AuthController@kirim');

Route::get('/data-table',function(){
    return view('halaman.datatable');
}
);

//CRUDA CAST


//CREATE data cast

//masuk ke form cast/ inputan ke cast
Route::get('/cast/create', 'CastController@create');

//untuk kirim inputan ke table cast
Route::POST('/cast', 'CastController@store');

//READ data cast

//tampil semua data cast
Route::get('/cast','CastController@index');
//tampil bagian detail cast
Route::get('/cast/{id}','CastController@show');

//UPDATE data cast

//masuk ke form cast berdasarkan id
Route :: get('/cast/{id}/edit', 'CastController@edit');
//Untuk kirim data update berdasar id
Route :: put('/cast/{id}', 'CastController@update');

//DELETE data cast

//delete data berdasarkan id
Route :: delete('/cast/{id}', 'CastController@destroy');