<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    function daftar(){
        return view('halaman.form');
    }

    function kirim(Request $request ){
        $firstname = $request['namadepan'];
        $lastname = $request['namabelakang'];

        return view('halaman.selamat', compact('firstname','lastname'));
    }
}
