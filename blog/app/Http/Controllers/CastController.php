<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:10',
        ],
        [
            'nama.required'=> 'Nama tidak boleh kosong',
            'umur.required'=> 'Umur tidak boleh kosong',
            'bio.required'=> 'Bio tidak boleh kosong',
            'bio.min'=> 'Minimal 6 huruf',
        ]
        );
        DB::table('cast')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);

        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));

    }

    public function show($id){
        $cast =  DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', compact('cast'));
    }

    public function edit($id){
        $cast =  DB::table('cast')->where('id', $id)->first();

        return view('cast.update', compact('cast'));
    }

    public function update(Request $request, $id){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:10',
        ],
        [
            'nama.required'=> 'Nama tidak boleh kosong',
            'umur.required'=> 'Umur tidak boleh kosong',
            'bio.required'=> 'Bio tidak boleh kosong',
            'bio.min'=> 'Minimal 6 huruf',
        ]
        );
        $affected = DB::table('cast')
              ->where('id', 1)
              ->update(
                  [
                      'nama'=>$request['nama'],
                      'umur'=>$request['umur'],
                      'bio'=>$request['bio'],
                  ]
              );
              
              return redirect ('/cast');
    }

    public function destroy($id){
        $deleted = DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
